package com.kirk.lvc;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Simple implemetation of the LVC Map based on a LinkedHashMap
 */
public class LVCLinkedHashMapImpl<K,V> implements Cache<K,V>{

    private final LinkedHashMap<K,V> map;

    public LVCLinkedHashMapImpl(final int maxSize){
        if(maxSize < 1)
            throw new IllegalArgumentException("max size > 0");

        this.map = new LinkedHashMap<K,V>(){

            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > maxSize;
          }
        };
    }

    @Override
    public void put(K key, V value) {
        // we make sure the item is put to the top of the list
        // by removing it (if present) before putting it back
        if(map.containsKey(key))
            map.remove(key);

        map.put(key,value);
    }

    @Override
    public V get(K key) {
        if(map.containsKey(key)){

            // put the value to the top of the list after
            // a get by removing it and putting it back
            V value = map.remove(key);
            map.put(key,value);
            return value;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        // to string delegated off
        return FormatUtil.toString(map.entrySet());
    }
}
