# LVC Cache

Create a Least Value Cache (LVC) for the most recently used elements. Create a data structure and associated code to fit the following criteria:

- The LVC must maintain a set of key-value pairs
- The LVC size is bounded by a limit parameter passed to the structure as an arguement
- The last element is evicted if the cache size exceeds the limit
- The LVC must preserve the access order fo the keys. A key becomes recently ised if a "get" or "put" operation is performed on the key
- Provide a toString() method that prints the key-value pairs contained in the LVC in order
- Make Thread Safe