package com.kirk.lvc;

/**
 * General contract for the cache includes only get and put methods
 */
public interface Cache<K,V> {

    void put(K key, V value);

    V get(K key);
}
