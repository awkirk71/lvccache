package com.kirk.lvc;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class FormatUtil {

    private FormatUtil(){}

    /**
     * Implementation of the LVC Map will vary but the toString method must remain the
     * same for all implementations, so it is pulled out here into a helper function
     */
    public static <K,V> String toString(Collection<Map.Entry<K,V>> entryCollection){
        final StringBuilder buff = new StringBuilder();
        new ArrayDeque<>(entryCollection)
                .descendingIterator()
                .forEachRemaining((entry)->
                    buff.append((buff.length() > 0)?",":"")
                    .append("<")
                    .append(entry.getKey())
                    .append(",")
                    .append(entry.getValue())
                    .append(">")
        );
        return buff.toString();
    }
}
