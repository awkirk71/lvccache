package com.kirk.lvc;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Thread safety provided by a lock decorator
 */
public class ThreadSafeCacheDecorator<K,V> implements Cache<K,V> {

    private final Cache<K,V> next;

    private final Lock lock = new ReentrantLock();

    public ThreadSafeCacheDecorator(Cache<K,V> next){
        this.next = next;
    }

    @Override
    public void put(K key, V value) {
        try{
            lock.lock();
            next.put(key,value);
        } finally{
            lock.unlock();
        }

    }

    @Override
    public V get(K key) {
        try{
            lock.lock();
            return next.get(key);
        } finally{
            lock.unlock();
        }
    }

    @Override
    public String toString() {
        try{
            lock.lock();
            return next.toString();
        } finally{
            lock.unlock();
        }
    }
}
