package com.kirk.lvc;

public class Caches {

    private Caches(){}

    public static <K,V> Cache<K,V> newThreadSafeCache(int maxSize){
        return new ThreadSafeCacheDecorator<>( new LVCLinkedHashMapImpl<>(maxSize) );
    }
}
