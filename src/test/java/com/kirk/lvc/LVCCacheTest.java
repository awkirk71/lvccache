package com.kirk.lvc;


import org.junit.Assert;
import org.junit.Test;

public class LVCCacheTest {

    @Test
    public void exampleTest() {
        Cache<String,String> cache = Caches.newThreadSafeCache(3);
        cache.put("A","Apple");
        cache.put("B","Battle");
        cache.put("C","Code");
        Assert.assertEquals("<C,Code>,<B,Battle>,<A,Apple>", cache.toString());

        Assert.assertEquals("Apple", cache.get("A"));
        Assert.assertEquals("<A,Apple>,<C,Code>,<B,Battle>", cache.toString());

        cache.put("E","Eagle");
        Assert.assertEquals("<E,Eagle>,<A,Apple>,<C,Code>", cache.toString());

        cache.put("C","Carrot");
        Assert.assertEquals("<C,Carrot>,<E,Eagle>,<A,Apple>", cache.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadSize() {
        Cache<String,String> cache = Caches.newThreadSafeCache(0);
    }
}
